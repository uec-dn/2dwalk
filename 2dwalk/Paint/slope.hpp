﻿#pragma once
#ifndef SLOPE_H
#define SLOPE_H
#include "object.hpp"

#include <QSize>
#include <QPoint>

namespace Paint {
	class Slope : public Object {
		Q_OBJECT

	public:
		Slope(QObject * parent = Q_NULLPTR)
			: Object(parent)
		{
		}

		void draw(QPainter &, bool showRect) override;
		bool collideWith(const QPoint &) override;
		void setCursor(QCursor &, const QPoint &) override;
		void dragBegin(const QPoint &) override;
		void drag(const QPoint &start, const QPoint &current) override;
		void onCursorMoved(const QPoint &) override;
		void onCursorLeft(const QPoint &) override;

		void setPosition(const QPoint &);
		void setPosition(const QPointF &);
		void setGamma(double);

		template <typename T>
		void setWidth(const T &v)
		{
			size_.setWidth(static_cast<qreal>(v));
			setHeight();
			emit slopeWidthChanged(size_.width());
		}
		const QSizeF &size() const
		{
			return size_;
		}
		QRect rect() const
		{
			auto left = static_cast<int>(position_.x());
			auto bottom = static_cast<int>(position_.y());
			auto width = static_cast<int>(size_.width());
			auto height = static_cast<int>(size_.height());
			return QRect(left, bottom - height, width, height);
		}
		int width() const
		{
			return static_cast<int>(size_.width());
		}
		double gamma() const
		{
			return gamma_;
		}

	private:
		enum class CollisionType {
			None,
			LeftResize,
			RightResize,
			Move,
		};

		void setHeight()
		{
			size_.setHeight(static_cast<qreal>(size_.width()) * std::tan(gamma_));
		}

		CollisionType getCollisionType(const QPoint &p)
		{
			auto rect = this->rect();

			if (std::abs(p.x() - rect.left()) <= 10) {
				return CollisionType::LeftResize;
			} else if (std::abs(p.x() - rect.right()) <= 10) {
				return CollisionType::RightResize;
			}

			return CollisionType::Move;
		}

		QSizeF size_;
		QPointF position_;

		CollisionType dragType_;
		double dragBasingWidth_;
		QPointF dragBasingPoint_;

		CollisionType cursorCollisionType_ = CollisionType::None;

		// Simulator
		double gamma_;

	signals:
		// Will be emitted when position is changed without calling setPosition, e.g. dragging.
		void moved(const QPoint &oldPos, const QPoint &newPos);
		void slopeWidthChanged(double);
	};
}
#endif // SLOPE_H