﻿#include "manager.hpp"

#include <QPainter>
#include <QShowEvent>
#include <QResizeEvent>
#include <QMouseEvent>

Paint::Manager::Manager(QWidget * parent)
	: QWidget(parent)
	, slope_(this)
	, model_(this)
	, modelDummy_(this)
{
	ui.setupUi(this);

	connect(
		&slope_, &Slope::moved,
		this, &Manager::slopeMoved);
	//connect(
	//	&slope_, &Slope::slopeWidthChanged,
	//	model_, &Model::setSlopeWidth);
}

Paint::Manager::~Manager()
{
}

void Paint::Manager::paintEvent(QPaintEvent *)
{
	QPainter painter(this);

	// Enable antialiasing
	painter.setRenderHint(QPainter::Antialiasing);

	// Draw contents
	slope_.draw(painter, showRectangle_);

	if (loopEnabled_) {
		auto height = this->height();
		painter.setClipRect(slope_.rect().left(), 0, slope_.width(), height);

		model_.draw(painter, showRectangle_);

		if(showDummy_){
			modelDummy_.draw(painter, showRectangle_);
		}
	}else{
		model_.draw(painter, showRectangle_);
	}
}

void Paint::Manager::showEvent(QShowEvent *event)
{
	// Set default positions of contents

	auto size = this->size();

	slope_.setWidth(size.width() - 20);
	slope_.setPosition(QPoint(10, size.height() - 10));
}

void Paint::Manager::resizeEvent(QResizeEvent *event)
{
	event->size();
}

void Paint::Manager::mouseMoveEvent(QMouseEvent *event)
{
	if (mousePressing_) {
		if (cursorFocusedObject_ != nullptr) {
			auto pos = event->pos();

			cursorFocusedObject_->drag(dragBasingPoint_, pos);
			adjustModelPosition(model_);

			if (loopEnabled_ && cursorFocusedObject_ == &model_) {
				auto modelRect = model_.calcContainingRect();
				auto modelDummyRect = modelDummy_.calcContainingRect();
				auto slopeRect = slope_.rect();

				if (modelRect.right() < slopeRect.left()
					|| modelRect.left() > slopeRect.right()
					|| (
						pos.x() >= slopeRect.left() && pos.x() <= slopeRect.right()
						&& modelDummyRect.contains(pos)
					))
				{
					auto x = model_.relativePos().x();

					model_.setRelativeX(modelDummy_.relativePos().x());
					modelDummy_.setRelativeX(x);

					adjustModelPosition(model_);
					adjustModelPosition(modelDummy_);

					model_.dragEnd(pos);
					model_.dragBegin(pos);
					model_.drag(pos, pos);

					dragBasingPoint_ = pos;
				}
			}

			setUpLoopState();
			update();

			emit stanceLegPosChanged(model_.relativePos().x());
		}
	} else {
		auto pos = event->pos();
		QCursor cursor(Qt::ArrowCursor);

		auto object = getCollidingObject(pos);

		if (object == &modelDummy_) {
			double x = model_.relativePos().x();

			model_.setRelativeX(modelDummy_.relativePos().x());
			modelDummy_.setRelativeX(x);

			adjustModelPosition(model_);
			adjustModelPosition(modelDummy_);

			object = &model_;
		}

		if (cursorFocusedObject_ != object) {
			if (cursorFocusedObject_ != nullptr) {
				cursorFocusedObject_->onCursorLeft(pos);
			}
			if (object != nullptr) {
				object->onCursorEntered(pos);
			}
		}

		if (object != nullptr) {
			object->setCursor(cursor, pos);
			object->onCursorMoved(pos);
		}

		cursorFocusedObject_ = object;

		setCursor(cursor);
		update();
	}
}

void Paint::Manager::mousePressEvent(QMouseEvent *event)
{
	mousePressing_ = true;
	dragBasingPoint_ = event->pos();

	if (cursorFocusedObject_ != nullptr) {
		cursorFocusedObject_->dragBegin(dragBasingPoint_);
	}
}

void Paint::Manager::mouseReleaseEvent(QMouseEvent *event)
{
	mousePressing_ = false;
	mouseMoveEvent(event);

	if (cursorFocusedObject_ != nullptr) {
		cursorFocusedObject_->dragEnd(event->pos());
	}
}

void Paint::Manager::leaveEvent(QEvent *event)
{
	if (event->type() == QEvent::Leave && cursorFocusedObject_ != nullptr) {
		cursorFocusedObject_->onCursorLeft(mapFromGlobal(QCursor::pos()));
		cursorFocusedObject_ = nullptr;
		update();
	}
}

void Paint::Manager::adjustModelPosition(Model &model)
{
	auto gamma = slope_.gamma();
	auto x = model.relativePos().x() - slope_.width();
	auto r = model.footRadius();

	auto y = (-r + x * std::sin(gamma)) / std::cos(gamma);
	model.setRelativeY(y);
}

void Paint::Manager::setUpLoopState()
{
	if (!loopEnabled_) {
		showDummy_ = false;
		return;
	}

	auto modelRect = model_.calcContainingRect();
	auto slopeRect = slope_.rect();

	if (modelRect.left() <= slopeRect.left() && modelRect.right() >= slopeRect.right()) {
		showDummy_ = false;
		return;
	}

	if (modelRect.left() <= slopeRect.left()) {
		showDummy_ = true;
		modelDummy_.setRelativeX(slope_.width() + model_.relativePos().x());
		adjustModelPosition(modelDummy_);
	} else if(modelRect.right() >= slopeRect.right()) {
		showDummy_ = true;
		modelDummy_.setRelativeX(model_.relativePos().x() - slope_.width());
		adjustModelPosition(modelDummy_);
	} else {
		showDummy_ = false;
		return;
	}

	if (modelRect.right() < slopeRect.left()
		|| modelRect.left() > slopeRect.right())
	{
		auto x = model_.relativePos().x();

		model_.setRelativeX(modelDummy_.relativePos().x());
		modelDummy_.setRelativeX(x);

		adjustModelPosition(model_);
		adjustModelPosition(modelDummy_);
	}
}

void Paint::Manager::setAutoStop(bool v)
{
	autoStop_ = v;
}

void Paint::Manager::setLoop(bool v)
{
	loopEnabled_ = v;

	if (v) {
		auto modelRect = model_.calcContainingRect();
		auto slopeRect = slope_.rect();

		if (modelRect.right() < slopeRect.left()) {
			model_.setRelativeX(model_.relativePos().x() - (modelRect.left() - slopeRect.left()));
			adjustModelPosition(model_);
		} else if (modelRect.left() > slopeRect.right()) {
			model_.setRelativeX(slopeRect.width() - (modelRect.right() - slopeRect.left() - model_.relativePos().x()));
			adjustModelPosition(model_);
		}
	}

	setUpLoopState();
	update();
}

void Paint::Manager::setGamma(double v)
{
	slope_.setGamma(v);

	model_.setGamma(v);
	adjustModelPosition(model_);

	modelDummy_.setGamma(v);
	adjustModelPosition(modelDummy_);

	setUpLoopState();

	update();
}

void Paint::Manager::setTheta1(double v)
{
	model_.setTheta1(v);
	modelDummy_.setTheta1(v);

	setUpLoopState();

	update();
}

void Paint::Manager::setTheta2(double v)
{
	model_.setTheta2(v);
	modelDummy_.setTheta2(v);

	setUpLoopState();

	update();
}

void Paint::Manager::setModelLegLength(double v)
{
	model_.setLegLength(v);
	modelDummy_.setLegLength(v);

	setUpLoopState();

	update();
}

void Paint::Manager::setModelFootRadius(double v)
{
	model_.setFootRadius(v);
	adjustModelPosition(model_);

	modelDummy_.setFootRadius(v);
	adjustModelPosition(modelDummy_);

	setUpLoopState();

	update();
}

void Paint::Manager::setModelHipRadius(double v)
{
	model_.setHipRadius(v);
	modelDummy_.setHipRadius(v);

	setUpLoopState();

	update();
}

void Paint::Manager::setStanceLegPos(double v)
{
	model_.setRelativeX(v);
	adjustModelPosition(model_);

	modelDummy_.setRelativeX(v);
	adjustModelPosition(modelDummy_);

	setUpLoopState();

	update();
}

void Paint::Manager::setStanceLegColor(const QColor & v)
{
	model_.setStanceLegColor(v);
	modelDummy_.setStanceLegColor(v);
	update();
}

void Paint::Manager::setSwingLegColor(const QColor & v)
{
	model_.setSwingLegColor(v);
	modelDummy_.setSwingLegColor(v);
	update();
}

void Paint::Manager::slopeMoved(const QPoint & oldPos, const QPoint & newPos)
{
	model_.setBasingPoint(newPos);
	modelDummy_.setBasingPoint(newPos);
	update();
}

void Paint::Manager::footContact()
{
	model_.footContact();
	adjustModelPosition(model_);

	modelDummy_.footContact();
	adjustModelPosition(modelDummy_);

	emit stanceLegPosChanged(model_.relativePos().x());
}

void Paint::Manager::setShowRectangle(bool v)
{
	showRectangle_ = v;
	update();
}

bool Paint::Manager::modelReachedToEnd()
{
	return autoStop_ && (model_.calcContainingRect().right() >= slope_.rect().right());
}
