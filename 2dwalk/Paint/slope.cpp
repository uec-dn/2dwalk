﻿#include "slope.hpp"

#include <cmath>

#include <QPainter>

void Paint::Slope::draw(QPainter &painter, bool showRect)
{
	QPointF rightTop(
		position_.x() + static_cast<qreal>(size_.width()),
		position_.y() - static_cast<qreal>(size_.width()) * std::tan(gamma_));

	painter.drawLine(QPointF(position_.x(), rightTop.y()), QPointF(rightTop.x(), position_.y()));
	painter.drawLine(QPointF(position_.x(), position_.y()), QPointF(rightTop.x(), position_.y()));

	if (showRect) {
		switch (cursorCollisionType_) {
		case CollisionType::None:
			break;
		case CollisionType::LeftResize:
			painter.drawRect(QRect(position_.x() - 10, rightTop.y() - 10, 20, size_.height() + 20));
			break;
		case CollisionType::RightResize:
			painter.drawRect(QRect(rightTop.x() - 10, rightTop.y() - 10, 20, size_.height() + 20));
			break;
		case CollisionType::Move:
			painter.drawRect(rect().marginsAdded(QMargins(0, 10, 0, 10)));
			break;
		default:
			Q_UNREACHABLE();
		}
	}
}

bool Paint::Slope::collideWith(const QPoint &pt)
{
	auto rect = this->rect();
	rect.setLeft(rect.left() - 10);
	rect.setRight(rect.right() + 10);
	rect.setTop(rect.top() - 10);
	rect.setBottom(rect.bottom() + 10);
	return rect.contains(pt);
}

void Paint::Slope::setCursor(QCursor &cursor, const QPoint &pt)
{
	switch (getCollisionType(pt)) {
	case CollisionType::LeftResize:
	case CollisionType::RightResize:
		cursor.setShape(Qt::SizeHorCursor);
		return;
	case CollisionType::Move:
		cursor.setShape(Qt::SizeAllCursor);
		return;
	default:
		Q_UNREACHABLE();
	}
}

void Paint::Slope::dragBegin(const QPoint &p)
{
	dragType_ = getCollisionType(p);

	switch (dragType_) {
	case CollisionType::LeftResize:
		dragBasingPoint_ = position_;
		dragBasingWidth_ = size_.width();
		break;
	case CollisionType::RightResize:
		dragBasingWidth_ = size_.width();
		break;
	case CollisionType::Move:
		dragBasingPoint_ = position_;
		break;
	default:
		Q_UNREACHABLE();
	}
}

void Paint::Slope::drag(const QPoint & start, const QPoint & current)
{
	switch (dragType_) {
	case CollisionType::LeftResize:
		position_.setX(dragBasingPoint_.x() + (current.x() - start.x()));
		size_.setWidth(dragBasingWidth_ - (current.x() - start.x()));
		setHeight();
		setPosition(position_);
		break;
	case CollisionType::RightResize:
		size_.setWidth(dragBasingWidth_ + (current.x() - start.x()));
		setHeight();
		setPosition(position_);
		break;
	case CollisionType::Move:
		position_.setX(dragBasingPoint_.x() + (current.x() - start.x()));
		position_.setY(dragBasingPoint_.y() + (current.y() - start.y()));
		setPosition(position_);
		break;
	default:
		Q_UNREACHABLE();
	}
}

void Paint::Slope::onCursorMoved(const QPoint &p)
{
	cursorCollisionType_ = getCollisionType(p);
}

void Paint::Slope::onCursorLeft(const QPoint &)
{
	cursorCollisionType_ = CollisionType::None;
}

void Paint::Slope::setPosition(const QPoint &v)
{
	emit moved(position_.toPoint(), v);
	position_ = v;
}

void Paint::Slope::setPosition(const QPointF &v)
{
	emit moved(position_.toPoint(), v.toPoint());
	position_ = v;
}

void Paint::Slope::setGamma(double v)
{
	gamma_ = v;
	setHeight();
}
