﻿#pragma once
#ifndef MODEL_H
#define MODEL_H
#include "object.hpp"

#include <tuple>

#include <QPoint>

namespace Paint {
	class Model : public Object {
		Q_OBJECT

	public:
		Model(QObject * parent = Q_NULLPTR)
			: Object(parent)
		{
		}

		void draw(QPainter &, bool showRect) override;
		bool collideWith(const QPoint &) override;
		void setCursor(QCursor &, const QPoint &) override;
		void dragBegin(const QPoint &) override;
		void drag(const QPoint &start, const QPoint &current) override;
		void onCursorEntered(const QPoint &) override;
		void onCursorLeft(const QPoint &) override;

		void setBasingPoint(const QPointF &);
		void setRelativeX(double);
		void setRelativeY(double);
		void setFootRadius(double);
		void setHipRadius(double);
		void setLegLength(double);
		void setTheta1(double);
		void setTheta2(double);
		void setGamma(double);
		void setStanceLegColor(const QColor &);
		void setSwingLegColor(const QColor &);

		void footContact();

		const QPointF &basingPoint() const
		{
			return basingPoint_;
		}
		const QPointF &relativePos() const
		{
			return relativePos_;
		}
		QPointF absolutePos() const
		{
			return basingPoint_ + relativePos_;
		}
		double footRadius() const
		{
			return footRadius_;
		}
		std::tuple<QPointF, QPointF> calcHipAndSwingLegPos() const
		{
			QPointF stancePos = absolutePos();

			auto l = legLength_;

			QPointF hipPos(
				stancePos.x() - l * std::sin(theta1_ - gamma_),
				stancePos.y() - l * std::cos(theta1_ - gamma_));
			QPointF swingPos(
				hipPos.x() + l * std::sin(theta1_ - (gamma_ + theta2_)),
				hipPos.y() + l * std::cos(theta1_ - (gamma_ + theta2_)));

			return std::make_tuple(hipPos, swingPos);
		}
		QRectF calcContainingRect() const
		{
			QPointF hip, swing, stance = basingPoint_ + relativePos_;
			std::tie(hip, swing) = calcHipAndSwingLegPos();

			hip -= QPointF(hipRadius_, hipRadius_);
			swing -= QPointF(footRadius_, footRadius_);
			stance -= QPointF(footRadius_, footRadius_);

			auto hipSize = std::max(1.0, hipRadius_ * 2.0);
			auto footSize = std::max(1.0, footRadius_ * 2.0);

			return QRectF(hip, QSizeF(hipSize, hipSize))
				.united(QRectF(swing, QSizeF(footSize, footSize)))
				.united(QRectF(stance, QSizeF(footSize, footSize)));
		}

	private:
		QPointF basingPoint_, relativePos_, dragBasingPoint_;
		double footRadius_, hipRadius_, legLength_;
		QColor stanceColor_, swingColor_;

		bool cursorFocused_ = false;

		// Simulator
		double theta1_, theta2_, gamma_;
	};
}
#endif // MODEL_H