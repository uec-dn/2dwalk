#pragma once

#include <QObject>
#include <QPainter>
#include <QCursor>

namespace Paint {
	class Object : public QObject
	{
	public:
		Object(QObject * parent = Q_NULLPTR)
			: QObject(parent)
		{
		}

		virtual void draw(QPainter &, bool showRect) = 0;
		virtual bool collideWith(const QPoint &)
		{
			// Never collides
			return false;
		}
		virtual void setCursor(QCursor &, const QPoint &)
		{
		}
		virtual void dragBegin(const QPoint &)
		{
		}
		virtual void drag(const QPoint &start, const QPoint &current)
		{
		}
		virtual void dragEnd(const QPoint &)
		{
		}
		virtual void onCursorEntered(const QPoint &)
		{
		}
		virtual void onCursorMoved(const QPoint &)
		{
		}
		virtual void onCursorLeft(const QPoint &)
		{
		}
	};
}
