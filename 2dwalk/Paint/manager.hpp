﻿#pragma once
#ifndef MANAGER_H
#define MANAGER_H
#include <QWidget>
#include "ui_manager.h"

#include "slope.hpp"
#include "model.hpp"

namespace Paint {
	class Manager : public QWidget {
		Q_OBJECT

	public:
		Manager(QWidget * parent = Q_NULLPTR);
		~Manager();

	protected:
		void paintEvent(QPaintEvent *) override;
		void showEvent(QShowEvent *) override;
		void resizeEvent(QResizeEvent *) override;
		void mouseMoveEvent(QMouseEvent *) override;
		void mousePressEvent(QMouseEvent *) override;
		void mouseReleaseEvent(QMouseEvent *) override;
		void leaveEvent(QEvent *) override;

	private:
		Object *getCollidingObject(const QPoint &p)
		{
			if (model_.collideWith(p)) {
				return &model_;
			} else if (loopEnabled_ && showDummy_ && modelDummy_.collideWith(p)) {
				return &modelDummy_;
			} else if (slope_.collideWith(p)) {
				return &slope_;
			} else {
				return nullptr;
			}
		}
		void adjustModelPosition(Model &);
		void setUpLoopState();

		Ui::Manager ui;

		bool mousePressing_ = false;
		QPoint dragBasingPoint_;
		Object *cursorFocusedObject_ = nullptr;

		Slope slope_;
		Model model_, modelDummy_;
		bool showDummy_ = false;

		bool showRectangle_;
		bool autoStop_;
		bool loopEnabled_;

	signals:
		void stanceLegPosChanged(double);

	public slots:
		void setAutoStop(bool v);
		void setLoop(bool v);
		void setGamma(double v);
		void setTheta1(double v);
		void setTheta2(double v);
		void setModelLegLength(double v);
		void setModelFootRadius(double v);
		void setModelHipRadius(double v);
		void setStanceLegPos(double v);
		void setStanceLegColor(const QColor &v);
		void setSwingLegColor(const QColor &v);
		void slopeMoved(const QPoint &oldPos, const QPoint &newPos);
		void footContact();
		void setShowRectangle(bool);
		bool modelReachedToEnd();
	};
}
#endif // MANAGER_H