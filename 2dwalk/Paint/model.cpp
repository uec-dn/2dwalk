﻿#include "model.hpp"

#include <QPainter>

void Paint::Model::draw(QPainter &painter, bool showRect)
{
	QPointF stancePos = absolutePos();
	QPointF hipPos, swingPos;

	std::tie(hipPos, swingPos) = calcHipAndSwingLegPos();

	painter.setBrush(QBrush(QColor(0, 0, 0, 0)));
	painter.setPen(QColor(0, 0, 0));

	{
		// Draw legs

		auto sin = std::sin(theta1_ - gamma_);
		auto cos = std::cos(theta1_ - gamma_);

		painter.drawLine(
			stancePos - QPointF(footRadius_ * sin, footRadius_ * cos),
			hipPos + QPointF(hipRadius_ * sin, hipRadius_ * cos));

		sin = std::sin(theta1_ - (theta2_ + gamma_));
		cos = std::cos(theta1_ - (theta2_ + gamma_));

		painter.drawLine(
			swingPos - QPointF(footRadius_ * sin, footRadius_ * cos),
			hipPos + QPointF(hipRadius_ * sin, hipRadius_ * cos));
	}

	painter.drawEllipse(hipPos, hipRadius_, hipRadius_);

	painter.setPen(stanceColor_);
	painter.drawEllipse(stancePos, footRadius_, footRadius_);

	painter.setPen(swingColor_);
	painter.drawEllipse(swingPos, footRadius_, footRadius_);

	painter.setPen(QColor(0, 0, 0));

	if (showRect && cursorFocused_) {
		painter.drawRect(calcContainingRect());
	}
}

bool Paint::Model::collideWith(const QPoint &p)
{
	return calcContainingRect().contains(p);
}

void Paint::Model::setCursor(QCursor &cursor, const QPoint &)
{
	cursor.setShape(Qt::SizeAllCursor);
}

void Paint::Model::dragBegin(const QPoint &)
{
	dragBasingPoint_ = relativePos_;
}

void Paint::Model::drag(const QPoint & start, const QPoint & current)
{
	relativePos_.setX(dragBasingPoint_.x() + (current.x() - start.x()));
}

void Paint::Model::onCursorEntered(const QPoint &)
{
	cursorFocused_ = true;
}

void Paint::Model::onCursorLeft(const QPoint &)
{
	cursorFocused_ = false;
}

void Paint::Model::setBasingPoint(const QPointF &v)
{
	basingPoint_ = v;
}

void Paint::Model::setRelativeX(double v)
{
	relativePos_.setX(v);
}

void Paint::Model::setRelativeY(double v)
{
	relativePos_.setY(v);
}

void Paint::Model::setFootRadius(double v)
{
	footRadius_ = v;
}

void Paint::Model::setHipRadius(double v)
{
	hipRadius_ = v;
}

void Paint::Model::setLegLength(double v)
{
	legLength_ = v;
}

void Paint::Model::setTheta1(double v)
{
	theta1_ = v;
}

void Paint::Model::setTheta2(double v)
{
	theta2_ = v;
}

void Paint::Model::setGamma(double v)
{
	gamma_ = v;
}

void Paint::Model::setStanceLegColor(const QColor &v)
{
	stanceColor_ = v;
}

void Paint::Model::setSwingLegColor(const QColor &v)
{
	swingColor_ = v;
}

void Paint::Model::footContact()
{
	QPointF p;
	std::tie(std::ignore, p) = calcHipAndSwingLegPos();
	relativePos_ = p - basingPoint_;
}
