﻿#pragma once
#ifndef WINDOW_H
#define WINDOW_H
#include <QWidget>
#include "ui_window.h"

#include <QIntValidator>
#include <QDoubleValidator>
#include <QTimer>

#include "simulator.hpp"
#include "Utility/math.hpp"

namespace Configurator {
	class Window : public QWidget {
		Q_OBJECT

	public:
		Window(QWidget * parent = Q_NULLPTR);
		~Window();

	protected:
		void closeEvent(QCloseEvent *) override;
		void showEvent(QShowEvent *) override;

	private:
		Ui::Window ui;

		QIntValidator intValidator_;
		QDoubleValidator validator_;
		QTimer timer_;

		Simulator::Progress initial_;

		bool useRadians_;

		double fromDegree(double);
		double fromRadian(double);
		double textToRadian(const QString &);

		void changeTheta2()
		{
			double theta2 = 2 * ui.theta1->text().toDouble();

			ui.theta2->setText(doubleToString(theta2));

			emit theta2Edited(useRadians_
				? theta2
				: Utility::degToRad(theta2));
		}

		void changeTheta2Dot()
		{
			double theta1 = textToRadian(ui.theta1->text());
			double theta1Dot = textToRadian(ui.theta1Dot->text());
			double cos = std::cos(2 * theta1);
			double theta2DotRadian = (1 - cos) * theta1Dot;

			ui.theta2Dot->setText(
				doubleToString(useRadians_
					? theta2DotRadian
					: Utility::radToDeg(theta2DotRadian)));

			emit theta2DotEdited(useRadians_
				? theta2DotRadian
				: Utility::degToRad(theta2DotRadian));
		}

		static QString doubleToString(double v)
		{
			return QString::number(v, 'g', 8);
		}

	signals:
		void autoStopButtonToggled(bool);
		void loopButtonToggled(bool);
		void autoStep();
		void timeEdited(double t);
		void deltaTEdited(double dt);
		void gammaEdited(double rad);
		void theta1Edited(double rad);
		void theta1DotEdited(double);
		void theta2Edited(double rad);
		void theta2DotEdited(double);
		void legLengthEdited(double l);
		void footRadiusEdited(double r);
		void hipRadiusEdited(double r);
		void stancePosEdited(double);
		void stanceColorEdited(const QColor &);
		void swingColorEdited(const QColor &);
		void allowableErrorEdited(double);
		void stepButtonPushed();
		void initializeSimulation(const Simulator::Progress &);
		Simulator::Progress queryCurrentProgress();
		void showRectangle(bool);

	public slots:
		void simulationProgress(const Simulator::Progress &);
		void stancePosChanged(double);
		void stopExternally();

	private slots:
		void useRadiansCheckBoxToggled(bool, bool changeEditBox = true);
	};
}
#endif // WINDOW_H