﻿#include "window.hpp"

#include <QCloseEvent>
#include <QColorDialog>

#include <functional>

#include "Utility/math.hpp"

Configurator::Window::Window(QWidget * parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	validator_.setNotation(QDoubleValidator::ScientificNotation);

	ui.speed->setValidator(&validator_);
	ui.t->setValidator(&validator_);
	ui.delta_t->setValidator(&validator_);
	ui.theta1->setValidator(&validator_);
	ui.theta1Dot->setValidator(&validator_);
	ui.theta2->setValidator(&validator_);
	ui.theta2Dot->setValidator(&validator_);
	ui.gamma->setValidator(&validator_);
	ui.legLength->setValidator(&validator_);
	ui.footSize->setValidator(&validator_);
	ui.hipSize->setValidator(&validator_);
	ui.stanceLegPos->setValidator(&validator_);
	ui.allowableError->setValidator(&validator_);

	connect(
		&timer_, &QTimer::timeout,
		this, &Window::autoStep);

	connect(
		ui.startButton, &QPushButton::toggled,
		[&](bool checked) {
			ui.resetButton->setEnabled(true);

			if (checked) {
				ui.startButton->setText("Stop");
				initial_ = emit queryCurrentProgress();
				timer_.start(static_cast<int>(1000.0 / std::max(1.0, ui.speed->text().toDouble())));
				emit initializeSimulation(initial_);
			} else {
				ui.startButton->setText("Start");
				timer_.stop();
			}
		});
	connect(
		ui.resetButton, &QPushButton::pressed,
		[&]() {
			emit initializeSimulation(initial_);
		});
	connect(
		ui.stepButton, &QPushButton::pressed,
		this, &Window::stepButtonPushed);
	connect(
		ui.autoStopRadioButton, &QCheckBox::toggled,
		this, &Window::autoStopButtonToggled);
	connect(
		ui.loopRadioButton, &QCheckBox::toggled,
		this, &Window::loopButtonToggled);
	connect(
		ui.speed, &QLineEdit::textChanged,
		[&](const QString &text) {
		timer_.setInterval(static_cast<int>(1000.0 / std::max(1.0, ui.speed->text().toDouble())));
		});
	connect(
		ui.t, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit timeEdited(text.toDouble());
		});
	connect(
		ui.delta_t, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit deltaTEdited(text.toDouble());
		});
	connect(
		ui.theta1, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit theta1Edited(textToRadian(text));

			if (ui.theta2AutoCheckBox->isChecked()) {
				changeTheta2();
				changeTheta2Dot();
			}
		});
	connect(
		ui.theta1Dot, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit theta1DotEdited(textToRadian(text));

			if (ui.theta2AutoCheckBox->isChecked()) {
				changeTheta2Dot();
			}
		});
	connect(
		ui.theta2, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit theta2Edited(textToRadian(text));
		});
	connect(
		ui.theta2AutoCheckBox, &QCheckBox::toggled,
		[&](bool checked) {
			ui.theta2->setReadOnly(checked);

			if (checked && !ui.startButton->isChecked()) {
				changeTheta2();
			}
		});
	connect(
		ui.theta2Dot, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit theta2DotEdited(textToRadian(text));
		});
	connect(
		ui.theta2DotAutoCheckBox, &QCheckBox::toggled,
		[&](bool checked) {
			ui.theta2Dot->setReadOnly(checked);

			if (checked && !ui.startButton->isChecked()) {
				changeTheta2Dot();
			}
		});
	connect(
		ui.gamma, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit gammaEdited(textToRadian(text));
		});
	connect(
		ui.legLength, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit legLengthEdited(text.toDouble());
		});
	connect(
		ui.footSize, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit footRadiusEdited(text.toDouble());
		});
	connect(
		ui.hipSize, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit hipRadiusEdited(text.toDouble());
		});
	connect(
		ui.stanceLegPos, &QLineEdit::textEdited,
		[&](const QString &text) {
			emit stancePosEdited(text.toDouble());
		});
	connect(
		ui.allowableError, &QLineEdit::textChanged,
		[&](const QString &text) {
			emit allowableErrorEdited(text.toDouble());
		});
	connect(
		ui.useRadiansCheckBox, &QCheckBox::toggled,
		std::bind(&Window::useRadiansCheckBoxToggled, this, std::placeholders::_1, true));
	connect(
		ui.footColorCheckBox, &QCheckBox::toggled,
		[&](bool checked) {
			ui.stanceColorLabel->setEnabled(checked);
			ui.stanceColorEdit->setEnabled(checked);
			ui.stanceColorButton->setEnabled(checked);
			ui.swingColorLabel->setEnabled(checked);
			ui.swingColorEdit->setEnabled(checked);
			ui.swingColorButton->setEnabled(checked);

			if (checked) {
				emit stanceColorEdited(ui.stanceColorEdit->text());
				emit swingColorEdited(ui.swingColorEdit->text());
			} else {
				emit stanceColorEdited(Qt::black);
				emit swingColorEdited(Qt::black);
			}
		});
	connect(
		ui.stanceColorEdit, &QLineEdit::textChanged,
		[&](const QString &text) {
			emit stanceColorEdited(text);
		});
	connect(
		ui.stanceColorButton, &QPushButton::pressed,
		[&]() {
			auto color = QColorDialog::getColor(ui.stanceColorEdit->text(), this);
			ui.stanceColorEdit->setText(color.name());
		});
	connect(
		ui.swingColorEdit, &QLineEdit::textChanged,
		[&](const QString &text) {
			emit swingColorEdited(text);
		});
	connect(
		ui.swingColorButton, &QPushButton::pressed,
		[&]() {
			auto color = QColorDialog::getColor(ui.swingColorEdit->text(), this);
			ui.swingColorEdit->setText(color.name());
		});
	connect(
		ui.rectangleCheckBox, &QCheckBox::toggled,
		this, &Window::showRectangle);

	// Create as a free-standing window
	setWindowFlags(
		Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint
		| Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint);
}

Configurator::Window::~Window()
{
}

void Configurator::Window::closeEvent(QCloseEvent *event)
{
	// Prevent close event
	event->ignore();
}

void Configurator::Window::showEvent(QShowEvent *)
{
	useRadiansCheckBoxToggled(ui.useRadiansCheckBox->isChecked(), false);

	Simulator::Progress progress = {
		ui.t->text().toDouble(),
		ui.delta_t->text().toDouble(),
		textToRadian(ui.theta1->text()),
		textToRadian(ui.theta1Dot->text()),
		textToRadian(ui.theta2->text()),
		textToRadian(ui.theta2Dot->text()),
	};

	emit initializeSimulation(progress);

	emit autoStopButtonToggled(ui.autoStopRadioButton->isChecked());
	emit loopButtonToggled(ui.loopRadioButton->isChecked());
	emit gammaEdited(textToRadian(ui.gamma->text()));
	emit theta1Edited(progress.theta1);
	emit theta2Edited(progress.theta2);
	emit legLengthEdited(ui.legLength->text().toDouble());
	emit footRadiusEdited(ui.footSize->text().toDouble());
	emit hipRadiusEdited(ui.hipSize->text().toDouble());
	emit stancePosEdited(ui.stanceLegPos->text().toDouble());
	emit swingColorEdited(ui.swingColorEdit->text());
	emit stanceColorEdited(ui.stanceColorEdit->text());
	emit showRectangle(ui.rectangleCheckBox->isChecked());
	emit allowableErrorEdited(ui.allowableError->text().toDouble());
}

double Configurator::Window::fromDegree(double v)
{
	return useRadians_
		? Utility::degToRad(v)
		: v;
}

double Configurator::Window::fromRadian(double v)
{
	return useRadians_
		? v
		: Utility::radToDeg(v);
}

double Configurator::Window::textToRadian(const QString &v)
{
	return useRadians_
		? v.toDouble()
		: Utility::degToRad(v.toDouble());
}

void Configurator::Window::simulationProgress(const Simulator::Progress &p)
{
	ui.t->setText(doubleToString(p.time));
	ui.theta1->setText(doubleToString(fromRadian(p.theta1)));
	ui.theta1Dot->setText(doubleToString(fromRadian(p.theta1Dot)));
	ui.theta2->setText(doubleToString(fromRadian(p.theta2)));
	ui.theta2Dot->setText(doubleToString(fromRadian(p.theta2Dot)));
}

void Configurator::Window::stancePosChanged(double v)
{
	ui.stanceLegPos->setText(doubleToString(v));
}

void Configurator::Window::stopExternally()
{
	ui.startButton->setChecked(false);
}

void Configurator::Window::useRadiansCheckBoxToggled(bool v, bool changeEditBox)
{
	useRadians_ = v;

	if (v) {
		ui.theta1Label->setText("theta 1 [rad]");
		ui.theta2Label->setText("theta 2 [rad]");
		ui.gammaLabel->setText("gamma [rad]");

		if (changeEditBox) {
			ui.theta1->setText(doubleToString(fromDegree(ui.theta1->text().toDouble())));
			ui.theta1Dot->setText(doubleToString(fromDegree(ui.theta1Dot->text().toDouble())));
			ui.theta2->setText(doubleToString(fromDegree(ui.theta2->text().toDouble())));
			ui.theta2Dot->setText(doubleToString(fromDegree(ui.theta2Dot->text().toDouble())));
			ui.gamma->setText(doubleToString(fromDegree(ui.gamma->text().toDouble())));
		}
	}else{
		ui.theta1Label->setText("theta 1 [deg]");
		ui.theta2Label->setText("theta 2 [deg]");
		ui.gammaLabel->setText("gamma [deg]");

		if(changeEditBox){
			ui.theta1->setText(doubleToString(fromRadian(ui.theta1->text().toDouble())));
			ui.theta1Dot->setText(doubleToString(fromRadian(ui.theta1Dot->text().toDouble())));
			ui.theta2->setText(doubleToString(fromRadian(ui.theta2->text().toDouble())));
			ui.theta2Dot->setText(doubleToString(fromRadian(ui.theta2Dot->text().toDouble())));
			ui.gamma->setText(doubleToString(fromRadian(ui.gamma->text().toDouble())));
		}
	}
}
