#include "mainwindow.hpp"

#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, configurator_(new Configurator::Window(this))
	, simulator_(this)
{
	ui.setupUi(this);

	connect(
		configurator_, &Configurator::Window::autoStopButtonToggled,
		ui.paintManager, &Paint::Manager::setAutoStop);
	connect(
		configurator_, &Configurator::Window::loopButtonToggled,
		ui.paintManager, &Paint::Manager::setLoop);
	connect(
		configurator_, &Configurator::Window::timeEdited,
		&simulator_, &Simulator::setTime);
	connect(
		configurator_, &Configurator::Window::deltaTEdited,
		&simulator_, &Simulator::setDeltaT);
	connect(
		configurator_, &Configurator::Window::gammaEdited,
		ui.paintManager, &Paint::Manager::setGamma);
	connect(
		configurator_, &Configurator::Window::gammaEdited,
		&simulator_, &Simulator::setGamma);
	connect(
		configurator_, &Configurator::Window::theta1Edited,
		ui.paintManager, &Paint::Manager::setTheta1);
	connect(
		configurator_, &Configurator::Window::theta1Edited,
		&simulator_, &Simulator::setTheta1);
	connect(
		configurator_, &Configurator::Window::theta1DotEdited,
		&simulator_, &Simulator::setTheta1Dot);
	connect(
		configurator_, &Configurator::Window::theta2Edited,
		ui.paintManager, &Paint::Manager::setTheta2);
	connect(
		configurator_, &Configurator::Window::theta2Edited,
		&simulator_, &Simulator::setTheta2);
	connect(
		configurator_, &Configurator::Window::theta2DotEdited,
		&simulator_, &Simulator::setTheta2Dot);
	connect(
		configurator_, &Configurator::Window::legLengthEdited,
		ui.paintManager, &Paint::Manager::setModelLegLength);
	connect(
		configurator_, &Configurator::Window::footRadiusEdited,
		ui.paintManager, &Paint::Manager::setModelFootRadius);
	connect(
		configurator_, &Configurator::Window::hipRadiusEdited,
		ui.paintManager, &Paint::Manager::setModelHipRadius);
	connect(
		configurator_, &Configurator::Window::stancePosEdited,
		ui.paintManager, &Paint::Manager::setStanceLegPos);
	connect(
		configurator_, &Configurator::Window::allowableErrorEdited,
		&simulator_, &Simulator::setAllowableError);
	connect(
		configurator_, &Configurator::Window::stanceColorEdited,
		ui.paintManager, &Paint::Manager::setStanceLegColor);
	connect(
		configurator_, &Configurator::Window::swingColorEdited,
		ui.paintManager, &Paint::Manager::setSwingLegColor);
	connect(
		configurator_, &Configurator::Window::showRectangle,
		ui.paintManager, &Paint::Manager::setShowRectangle);

	connect(
		configurator_, &Configurator::Window::initializeSimulation,
		[&](const Simulator::Progress &p) { simulator_.initialize(p); });
	connect(
		&simulator_, &Simulator::stepped,
		configurator_, &Configurator::Window::simulationProgress);
	connect(
		configurator_, &Configurator::Window::stepButtonPushed,
		[&]() { simulator_.step(); });
	connect(
		&simulator_, &Simulator::stepped,
		[&](const Simulator::Progress &p) {
			ui.paintManager->setTheta1(p.theta1);
			ui.paintManager->setTheta2(p.theta2);

			if (ui.paintManager->modelReachedToEnd()) {
				configurator_->stopExternally();
			}
		});
	connect(
		&simulator_, &Simulator::footContact,
		ui.paintManager, &Paint::Manager::footContact);
	connect(
		configurator_, &Configurator::Window::autoStep,
		this, &MainWindow::autoStep);
	connect(
		configurator_, &Configurator::Window::queryCurrentProgress,
		[&]() { return simulator_.currentProgress(); });
	connect(
		ui.paintManager, &Paint::Manager::stanceLegPosChanged,
		configurator_, &Configurator::Window::stancePosChanged);
}

MainWindow::~MainWindow()
{
	configurator_->close();

	delete configurator_;
}

void MainWindow::showEvent(QShowEvent *)
{
	configurator_->show();
}

void MainWindow::autoStep()
{
	simulator_.step();
}
