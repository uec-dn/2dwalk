﻿#include "simulator.hpp"

#include <cmath>
#include <array>

namespace {
	auto func = [](
		double theta1, double theta1Dot,
		double theta2, double theta2Dot,
		double gamma)
	{
		std::array<double, 4> result;

		result[0] = theta1Dot;
		result[1] = std::sin(theta1 - gamma);
		result[2] = theta2Dot;
		result[3] =
			-(std::cos(theta2) - 1.0) * result[1]
			+ theta1Dot * theta1Dot * std::sin(theta2)
			- std::sin(theta2 - theta1 + gamma);

		return result;
	};
}

Simulator::Simulator(QObject * parent)
	: QObject(parent)
{
}

Simulator::~Simulator()
{
}

void Simulator::initialize(const Progress &v)
{
	currentProgress_ = v;
	emit stepped(v);
}

void Simulator::step()
{
	auto theta1 = currentProgress_.theta1;
	auto theta1Dot = currentProgress_.theta1Dot;
	auto theta2 = currentProgress_.theta2;
	auto theta2Dot = currentProgress_.theta2Dot;
	auto dt = currentProgress_.deltaT;
	auto dt2 = dt / 2;
	auto dt6 = dt / 6;

	auto k1 = func(theta1, theta1Dot, theta2, theta2Dot, gamma_);
	auto k2 = func(
		theta1 + dt2 * k1[0], theta1Dot + dt2 * k1[1],
		theta2 + dt2 * k1[2], theta2Dot + dt2 * k1[3], gamma_);
	auto k3 = func(
		theta1 + dt2 * k2[0], theta1Dot + dt2 * k2[1],
		theta2 + dt2 * k2[2], theta2Dot + dt2 * k2[3], gamma_);
	auto k4 = func(
		theta1 + dt * k3[0], theta1Dot + dt * k3[1],
		theta2 + dt * k3[2], theta2Dot + dt * k3[3], gamma_);

	currentProgress_.time += dt;
	currentProgress_.theta1 += dt6 * (k1[0] + 2 * k2[0] + 2 * k3[0] + k4[0]);
	currentProgress_.theta1Dot += dt6 * (k1[1] + 2 * k2[1] + 2 * k3[1] + k4[1]);
	currentProgress_.theta2 += dt6 * (k1[2] + 2 * k2[2] + 2 * k3[2] + k4[2]);
	currentProgress_.theta2Dot += dt6 * (k1[3] + 2 * k2[3] + 2 * k3[3] + k4[3]);

	emit stepped(currentProgress_);

	if (std::abs((2 * currentProgress_.theta1 - currentProgress_.theta2) / currentProgress_.theta2) <= allowableError_
		&& currentProgress_.theta1 < 0.0
		&& 2 * currentProgress_.theta1Dot - currentProgress_.theta2Dot < 0.0)
	{
		emit footContact();

		theta1 = currentProgress_.theta1;
		theta1Dot = currentProgress_.theta1Dot;
		theta2 = currentProgress_.theta2;
		theta2Dot = currentProgress_.theta2Dot;

		currentProgress_.theta1 = -theta1;
		currentProgress_.theta1Dot = theta1Dot * std::cos(2.0 * theta1);
		currentProgress_.theta2 = -2.0 * theta1;
		currentProgress_.theta2Dot = std::cos(2.0 * theta1) * (1.0 - std::cos(2.0 * theta1)) * theta1Dot;

		emit stepped(currentProgress_);
	}
}

void Simulator::setTime(double v)
{
	currentProgress_.time = v;
}

void Simulator::setDeltaT(double v)
{
	currentProgress_.deltaT = v;
}

void Simulator::setGamma(double v)
{
	gamma_ = v;
}

void Simulator::setTheta1(double v)
{
	currentProgress_.theta1 = v;
}

void Simulator::setTheta1Dot(double v)
{
	currentProgress_.theta1Dot = v;
}

void Simulator::setTheta2(double v)
{
	currentProgress_.theta2 = v;
}

void Simulator::setTheta2Dot(double v)
{
	currentProgress_.theta2Dot = v;
}

void Simulator::setAllowableError(double v)
{
	allowableError_ = v;
}
