﻿#pragma once
#ifndef SIMULATOR_H
#define SIMULATOR_H
#include <QObject>

class Simulator : public QObject {
	Q_OBJECT

public:
	Simulator(QObject * parent = Q_NULLPTR);
	~Simulator();

	struct Progress {
		double time, deltaT;
		double theta1, theta1Dot, theta2, theta2Dot;
	};

	void initialize(const Progress &);
	const Progress &currentProgress() const
	{
		return currentProgress_;
	}

private:
	Progress currentProgress_;
	double gamma_;
	double allowableError_;

signals:
	void stepped(const Progress &);
	void footContact();

public slots:
	void step();
	void setTime(double);
	void setDeltaT(double);
	void setGamma(double);
	void setTheta1(double);
	void setTheta1Dot(double);
	void setTheta2(double);
	void setTheta2Dot(double);
	void setAllowableError(double);
};
#endif // SIMULATOR_H