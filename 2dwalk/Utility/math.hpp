#pragma once

#if defined(_MSC_VER)
#  define _USE_MATH_DEFINES
#endif

#include <cmath>

#if !defined(M_PI)
#  define M_PI 3.14159265358979323846
#endif

namespace Utility {
	inline double degToRad(double deg)
	{
		return deg * (M_PI / 180.0);
	}
	inline double radToDeg(double deg)
	{
		return deg * (180.0 / M_PI);
	}
}
