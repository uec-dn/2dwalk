#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include "ui_mainwindow.h"

#include <QPaintEvent>

#include "Configurator/window.hpp"
#include "simulator.hpp"

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

protected:
	void showEvent(QShowEvent *) override;

private:
	Ui::MainWindowClass ui;

	Configurator::Window *configurator_;

	Simulator simulator_;

public slots:
	void autoStep();
};

#endif // MAINWINDOW_H
